/* global config, angular, Faye */


angular.module('myApp.services', []).
  value('version', '0.1').
  factory('pubsub', function($rootScope, $http, $q){
    "use strict";

	return{
        onCreatedEntity: function(callback){
            var deferred = $q.defer();
            var result = $http.get('/api/config').
                success(function(result, status, headers, config){
                    var client = new Faye.Client(result.entityUri, {
                        timeout: 120
                    });
                    deferred.resolve(
                        client.subscribe('/createentity', function(entity){
                            callback(entity);
                        })
                    );
                });
            return deferred.promise;
		}
	};
  });
/*  socket.io service - we are using faye above now
  factory('socket', function($rootScope){
	var socket = io.connect();
	return {
		on: function(eventName, callback){
			socket.on(eventName, function(){
				var args = arguments;
				$rootScope.$apply(function(){
					callback.apply(socket, args);
				});
			});
		},
		emit: function(eventName, data, callback){
			socket.emit(eventName, data, function(){
				$rootScope.$apply(function(){
					if(callback){
						callback.apply(socket, args);
					}
				});
			});
		}
	}
  });
  */
