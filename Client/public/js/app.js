/*global angular */

// Declare app level module which depends on filters, and services
angular.module('myApp', ['angular-underscore', 'myApp.filters', 'myApp.services', 'myApp.directives']).
  config(['$routeProvider', function($routeProvider) {
    'use strict';
    $routeProvider
		.when('/',				{templateUrl:'partials/index',			controller: IndexCtrl})
        .when('/map',		    {templateUrl:'partials/map',			controller: MapCtrl})
        .when('/map/:id',		    {templateUrl:'partials/map',			controller: ChildMapCtrl})
		.when('/addEntity',		{templateUrl:'partials/addEntity',		controller:	AddEntityCtrl})
		.when('/detailsEntity/:id',	{templateUrl:'partials/detailsEntity',	controller:	DetailEntityCtrl})
		.when('/editEntity/:id',	{templateUrl:'partials/editEntity',		controller:	EditEntityCtrl})
		.when('/deleteEntity/:id',	{templateUrl:'partials/deleteEntity',	controller: DeleteEntityCtrl})
		.otherwise({redirectTo: '/'});
  }]);
