/*global OpenLayers, _ */

//function IndexCtrl($scope, $http, socket) {
function IndexCtrl($scope, $http, pubsub) {
    "use strict";
	$scope.currentTime = new Date();
	$scope.newentities = [];
    $scope.count = 0;
    var result = $http.get('/api/entities').
		success(function(result, status, headers, config){
            $scope.entities = result.entities;
		});

    pubsub.onCreatedEntity(function(entity){
		//console.log('onCreatedEntity received');
		$scope.$apply(function(){
			$scope.count = $scope.count + 1;
			$scope.newentities.push(entity);
		});
	});
}

function DetailEntityCtrl($scope, $http, $routeParams){
    "use strict";
	$http.get('/api/entity/' + $routeParams.id).
		success(function(data, status, headers, config){
			console.log(data);
			$scope.entity = data.entity;
		});
}

function AddEntityCtrl($scope, $http, $location){
    "use strict";
	$scope.form = {};
	$scope.submitEntity = function(){
		$http.post('/api/entity', $scope.form).
			success(function(data){
				console.log('entity added', data);
				$location.path('/');
			});
	};
}

function EditEntityCtrl($scope, $http, $location, $routeParams){
    "use strict";
	$scope.form = {};
	$http.get('/api/entity/' + $routeParams.id).
		success(function(data){
			$scope.form = data.entity;
		});

	$scope.editEntity = function(){
		$http.put('/api/entity/' + $routeParams.id, $scope.form).
			success(function(data){
				console.log('entity added', data);
				$location.path('#/detailEntity/' + $routeParams.id);
			});
	}
}

function DeleteEntityCtrl($scope, $http, $location, $routeParams){
    "use strict";
	$http.get('/api/entity/' + $routeParams.id).
		success(function(data){
			$scope.entity = data.entity;
		});
	$scope.deleteEntity = function(){
		$http.delete('/api/entity/' + $routeParams.id).
			success(function(data){
				$location.url('/');
			});
	};
}

function MapCtrl($scope, $http, pubsub) {
    "use strict";

    var map = new OpenLayers.Map({
        div: 'map'
    });
    //define our projection:
    var P4326 = new OpenLayers.Projection("EPSG:4326");
    var P900913 = new OpenLayers.Projection("EPSG:900913");

    var lyrOpenLayers = new OpenLayers.Layer.Google(
        "Google Hybrid",
        { type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20, "sphericalMercator": true }
    );
    map.addLayer(lyrOpenLayers); //always add this base first!

    //add marker layer next...
    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);

    //create the icon:
    var size = new OpenLayers.Size(21, 25);
    var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
    var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);



    //add initial marker:
    //markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(0, 0).transform(P4326, P900913), icon));
    function registerMarkerPopup(entity){
        var clonedIcon = icon.clone();
        var marker = new OpenLayers.Marker(new OpenLayers.LonLat(entity.longitude, entity.latitude).transform(P4326, P900913), clonedIcon)

        markers.addMarker(marker);

        marker.events.register('click', marker, function (evt) {

            $http.get('/api/entity/' + entity._id).
                success(function(data, status, headers, config){
                    $scope.currentItem = data.entity.childentities.length;
                    var html = "<p>Child Entities</p>";
                    html += "<a href='#/map/" + data.entity._id + "'>View</a>";
                    html += "<ul>";
                    _.each(data.entity.childentities, function(child){
                        html += "<li>" + child._id + "</li>";
                    });
                    html += "</ul>";
                    var popup = new OpenLayers.Popup.FramedCloud(
                        "Popup",
                        new OpenLayers.LonLat(entity.longitude, entity.latitude).transform(P4326, P900913),
                        null,
                        '<div style="color:#FF0000;">' + html + '</div>',
                        null,
                        true
                    );
                    map.addPopup(popup);

                });
        });
    }

    $scope.currentTime = new Date();
    $scope.entities = [];
    $scope.count = 0;

    var result = $http.get('/api/entities').
        success(function(result, status, headers, config){
            $scope.entities = result.entities;
            _.each(result.entities, function(entity) {
                $scope.count = result.entities.length;
                registerMarkerPopup(entity);
            });
        });

    pubsub.onCreatedEntity(function(entity){
        //console.log('onCreatedEntity received');
        $scope.$apply(function(){
            $scope.count = $scope.count + 1;
            $scope.entities.push(entity);

            registerMarkerPopup(entity);
            //var clonedIcon = icon.clone();
            //markers.addMarker(new OpenLayers.Marker(new OpenLayers.LonLat(entity.longitude, entity.latitude).transform(P4326, P900913), clonedIcon));
        });
    });

    //map.setCenter(new OpenLayers.LonLat(0, 0), 0);
    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.zoomToMaxExtent();
}

function ChildMapCtrl($scope, $routeParams, $http, pubsub) {
    "use strict";

    $scope.entities = [];
    $scope.count = 0;

    var map = new OpenLayers.Map({
        div: 'map'
    });
    //define our projection:
    var P4326 = new OpenLayers.Projection("EPSG:4326");
    var P900913 = new OpenLayers.Projection("EPSG:900913");

    var lyrOpenLayers = new OpenLayers.Layer.Google(
        "Google Hybrid",
        { type: google.maps.MapTypeId.HYBRID, numZoomLevels: 20, "sphericalMercator": true }
    );
    map.addLayer(lyrOpenLayers); //always add this base first!

    //add marker layer next...
    var markers = new OpenLayers.Layer.Markers("Markers");
    map.addLayer(markers);


    map.addControl(new OpenLayers.Control.LayerSwitcher());
    map.zoomToMaxExtent();


    function registerChildMarker(entity){

        //create the icon:
        var size = new OpenLayers.Size(21, 25);
        var offset = new OpenLayers.Pixel(-(size.w / 2), -size.h);
        var icon = new OpenLayers.Icon('http://www.openlayers.org/dev/img/marker.png', size, offset);

        var clonedIcon = icon.clone();
        var marker = new OpenLayers.Marker(new OpenLayers.LonLat(entity.longitude, entity.latitude).transform(P4326, P900913), clonedIcon)

        markers.addMarker(marker);
    }

    $http.get('/api/entity/' + $routeParams.id).
        success(function(data){
            $scope.currentItem = data.entity.childentities.length;
            $scope.entities = data.entity.childentities;
            _.each(data.entity.childentities, function(child){
               registerChildMarker(child);
            });
        });
}





