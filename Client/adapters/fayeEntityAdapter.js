/*global require*/
(function(){
	var faye = require('faye');

	exports.create = function(server){
		var bayeux = new faye.NodeAdapter({mount: '/entity', timeout: 45});
		bayeux.attach(server);
	
		bayeux.bind('handshake', function(clientId){
			console.log('handshake ' + clientId);
		});
	
		var subscription = bayeux.getClient().subscribe('/createentity', function(message){
			console.log('subscription received by bayeux client');
		});
	
		subscription.callback(function(){
			console.log('Subscription to createentity is now active!');
		});
	}

})();

