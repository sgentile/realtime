'use strict';

describe('IndexCtrl', function(){
	it('should get the newentities', function($rootScope){
		var scope = {
			newentities: []
		},
		http = {
			get:function(){
				return {
					success:function(){
						return;
					}
				};
			}
		},
		pubsub = {
			onCreatedEntity: function(){
				scope.newentities.push({});
			}
		},
			
		ctrl = new IndexCtrl(scope, http, pubsub);
		expect(scope.newentities.length).toBe(1);
	});
});
