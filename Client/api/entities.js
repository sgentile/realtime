/*
 * Serve JSON to our AngularJS client
 */
/*global require, console*/
var config = require('../config.js');

(function () {
    var mongoose = require('mongoose'),
        Schema = mongoose.Schema,
        ObjectId = Schema.ObjectId,
        uuid = require('node-uuid');  //https://github.com/broofa/node-uuid

    var EntitySchema = new Schema({
        latitude: {type: Number},
        longitude: {type: Number},
        category: {type: String},
        key: {type: String, default: uuid.v4()},
        timestamp: {type: Date},
        //childentities: [{type: Schema.Types.ObjectId, ref: 'ChildEntity'}]
        childentities: [ChildEntitySchema]
    });

    var ChildEntitySchema = new Schema({
        latitude: {type: Number},
        longitude: {type: Number},
        _entity: {type: Schema.Types.ObjectId, ref: 'Entity'},
        category: {type: String},
        timestamp: {type: Date}
    });

    //mongoose.connect('mongodb://localhost/entitydatabase');
    mongoose.connect(config.mongoDatabase);
    var Entity = mongoose.model('Entity', EntitySchema);
    var ChildEntity = mongoose.model('ChildEntity', ChildEntitySchema);

    /* setup some fake data to start */
    /*
     app.get('/api/entities', api.entities);
     app.get('/api/entity/:id', api.entity);
     app.post('/api/entity', api.addEntity);
     app.put('/api/entity/:id', api.editEntity);
     app.delete('/api/entity/:id', api.deleteEntity);
     */

    var self = this;

    self.entities = function (req, res) {
        var date1 = new Date();

        date1.setMinutes(date1.getMinutes() - 2); //two minute's ago

        Entity.find({"timestamp": {"$gt": date1}})
            .select({childentities: 0}) //excludes childentities
            .exec(function (err, entities) {
                if (err) {
                    window.console.log('error', err);
                    return;
                }
                //console.log('entities query', entities);
                res.json({entities: entities});
            });
    };

    self.entity = function (req, res) {
        console.log(req.params);
        var id = req.params.id;
        //findOne
        Entity.findOne({_id: id}, function (err, entity) {
            if (err) {
                console.log(err)
            }
            else {
                console.log(entity);
                res.json({entity: entity});
            }
        });
    };

    self.addEntity = function (req, res) {
        console.log(req.body);
        var newEntity = new Entity(req.body);
        newEntity.key = uuid.v4();

        newEntity.save(function (err, newEntity) {
            if (err) {
                console.log('error saving', err);
            }
            else {
                console.log(newEntity);
                res.json(newEntity);
            }
        });
    };

    self.editEntity = function (req, res) {
        delete req.body._id
        Entity.update({_id: req.params.id}, req.body, function (err, entity) {
            if (err) {
                console.log(err);
            }
            else {
                console.log(entity);
                res.json(entity);
            }
        });
    };

    self.deleteEntity = function (req, res) {
        Entity.remove({_id: req.params.id}, function (err) {
            if (!err) {
                res.json(true);
            }
            else {
                console.log(err);
                res.json(false);
            }
        });
    };

    self.getConfig = function(req, res){
        res.json(config);
    };

    exports.createRoutes = function (app) {
        app.get('/api/config', self.getConfig);
        app.get('/api/entities', self.entities);
        app.get('/api/entity/:id', self.entity);
        app.post('/api/entity', self.addEntity);
        app.put('/api/entity/:id', self.editEntity);
        app.delete('/api/entity/:id', self.deleteEntity);
    };

})();




