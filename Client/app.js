
/**
 * Module dependencies.
 */
/* global require */
var express = require('express'),
  //expose = require('express-expose'), //exposes config file!
  routes = require('./routes'),
  entities = require('./api/entities'),
  fayeEntityAdapter = require('./adapters/fayeEntityAdapter'), 
  http = require('http'),
  path = require('path'),
  app = module.exports = express(),
  server = http.createServer(app);

// Configuration
app.configure(function(){
	app.set('port', process.env.PORT || 3000);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'ejs');
	app.use(express.bodyParser()); //parses post data of the body
	app.use(express.methodOverride());
	app.use(require('less-middleware')({ src: __dirname + '/public' }));
	app.use(express.static(path.join(__dirname, 'public')));
	app.use(app.router);
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

// Routes

app.get('/', routes.index);
app.get('/partials/:name', routes.partials);

entities.createRoutes(app);


// redirect all others to the index (HTML5 history)
app.get('*', routes.index);

server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

fayeEntityAdapter.create(server);

