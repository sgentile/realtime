exports.saveAll = function( docs, callback ){
  var count = 0,
	  saved = [];
  docs.forEach(function(doc){
      doc.save(function(err, result){
          count++;
		  if(err){console.log(err);callback(err, saved);}
		  saved.push(result);
          if( count == docs.length ){
             callback(null, saved);
          }
      });
  });
}
