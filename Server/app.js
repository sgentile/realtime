/**
 * Module dependencies.
 */
/*global require*/
var config = require('./config'),
    mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId,
    uuid = require('node-uuid'),
    faye = require('faye'),
    helper = require('./helper');

//create a faye client to publish to faye
var client = new faye.Client(config.entityUri);


var EntitySchema = new Schema({
    latitude: {type: Number},
    longitude: {type: Number},
    category: {type: String},
    key: {type: String, default: uuid.v4()},
    timestamp: {type: Date},
    childentities: [ChildEntitySchema]
});

var ChildEntitySchema = new Schema({
    latitude: {type: Number},
    longitude: {type: Number},
    _entity: {type: Schema.Types.ObjectId, ref: 'Entity'},
    category: {type: String},
    timestamp: {type: Date}
});


mongoose.connect(config.mongoDatabase);
var Entity = mongoose.model('Entity', EntitySchema);
var ChildEntity = mongoose.model('ChildEntity', ChildEntitySchema);

function getRandomInRange(from, to, fixed) {
    "use strict";
    return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
    // .toFixed() returns string, so ' * 1' is a trick to convert to number
}

var createRecord = function () {
    "use strict";
    var entity = new Entity({
        latitude: getRandomInRange(-180, 180, 6),
        longitude: getRandomInRange(-90, 90, 6),
        category: "US",
        key: uuid.v4(),
        timestamp: new Date()
    });

    for (var i = 0; i < 10; i++) {
        var child = new ChildEntity({
            latitude: getRandomInRange(-180, 180, 6),
            longitude: getRandomInRange(-90, 90, 6),
            key: uuid.v4(),
            _entity: entity._id,
            timestamp: new Date()
        });
        entity.childentities.push(child);
    }
    helper.saveAll(entity.childentities, function () {

        entity.save(function (err, savedEntity) {
            if (err) {
                console.log('error saving', err);
            }
            else {
                console.log(savedEntity);

                client.publish('/createentity', savedEntity);
            }
        });
    });
};

//post data to Mongodb to represent data from a server
//go ahead and create one to start the process:
createRecord();
setInterval(function () {
    "use strict";
    createRecord();
}, 10000); //every 10 seconds for now


