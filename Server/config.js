/*global module */
//note: services.js also has a reference to localhost...
var config = {};
config.dnsIp = "localhost";
config.port = 3000;
config.entityUri = "http://localhost:3000/entity";
config.mongoDatabase = "mongodb://localhost/entitydatabase";
module.exports = config;
