This is a demo of using Mongodb, NodeJS, Mongoose, Express, and AngularJS on the client

To begin, you must have a working Mongodb running

There are 2 node applications here.

1. Client
2. Server

The Server simulates posting data to mongodb as well as publishing a 'faye' websocket server event.

goto the Server directory and run 'npm install' to get the packages the first time.

Then run 'node app' from within this directory

To start the client, goto the Client folder and type 'npm install' the first time, then run 'node app'.

Browse to localhost:3000
